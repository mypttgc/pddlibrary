﻿using Microsoft.SharePoint.Client;
using OfficeDevPnP.Core;
using OfficeDevPnP.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Microsoft.SharePoint.Client.WebParts;
using System.Globalization;

public static class PDDService
{
    private static string _username;
    private static string _password;
    private static string _siteUrl;
    public static string _pathXml;

    ///<summary>
    ///Initial values in Static Object
    ///</summary>
    ///<para>Username: test@group.com</para>
    ///<para>Password: xxxxxx</para>
    ///<para>SiteUrl: http://domain/subsite/</para>
    ///

    ///<summary>
    ///Initial values in Static Object
    ///</summary>
    ///<param name="pddUsername">Username: test@group.com</param>
    ///<param name="pddPassword">Password: xxxxxx</param>
    ///<param name="pddSiteurl">SiteUrl: http://domain/subsite/ </param>
    public static void Init(string pddUsername, string pddPassword, string pddSiteurl)
    {
        _username = pddUsername;
        _password = pddPassword;
        _siteUrl = pddSiteurl;
        _pathXml = $"{Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(PDDService)).Location)}\\template.webpart";
    }


    /// <summary>
    /// Create Page and Document Library is Site
    /// </summary>
    /// <param name="pddPageName">The name of page for create in site.</param>
    /// <param name="pddDocumentName">The name of document library for create in site.</param>
    /// <param name="pddWebPartTitleName">The name of title of WebPart</param>
    /// <param name="pddPathPageUrl">The url site of page for create.</param>
    /// <returns>Return Status Result</returns>
    public static string AddPageAndDocument(string pddPageName, string pddDocumentName, string pddWebPartTitleName,string pddPathPageUrl)
    {
        try
        {
            //using (ClientContext ctx = new ClientContext(_siteUrl)) //
            //{
            using (ClientContext ctx = new AuthenticationManager().GetAppOnlyAuthenticatedContext(_siteUrl, _username, _password))
            {
                //#region Authentication
                //SecureString passWord = new SecureString();
                //foreach (char c in _password.ToCharArray()) passWord.AppendChar(c);
                //ctx.Credentials = new SharePointOnlineCredentials(_username, passWord);
                //#endregion

                //#region Create Document Library
                ctx.Web.CreateDocumentLibrary(pddDocumentName, false, "");
                //#endregion

                //#region Create Wiki Page
                //ctx.Web.AddWikiPage(pddPathPageUrl, $"{pddPageName}.aspx");
                //string[] splitSite = _siteUrl.Split('/');
                //ctx.Web.AddLayoutToWikiPage(WikiPageLayout.OneColumn, $"/{splitSite[splitSite.Length - 2]}/{pddPathPageUrl}/{pddPageName}.aspx");
                //#endregion

                // ObjectPath s = new ObjectPath()
                //FolderCollection a = ctx.Web.RootFolder.Folders;
                //a.Add

                //foreach(var ss in a)
                //{
                //}


                #region Create Publishing Page
                ctx.Web.AddPublishingPage($"{pddPageName}", "BlankWebPartPage", "Page", false);
                #endregion

                #region Checkout PublishPage
                var file = ctx.Web.GetFileByServerRelativeUrl($"{pddPathPageUrl}{pddPageName}.aspx");
                file.CheckOut();
                ctx.ExecuteQuery();

                #endregion

                #region Read XML WebPart
                var webPartXml = System.IO.File.ReadAllText(_pathXml);
                var webPartXmlName = String.Format(webPartXml, pddDocumentName);
                #endregion

                #region Add WepPart to Page
                //WebPartEntity webpart = new WebPartEntity() { WebPartIndex = 0, WebPartTitle = pddWebPartTitleName, WebPartXml = webPartXmlName, WebPartZone = "" };
                //ctx.Web.AddWebPartToWikiPage($"/{splitSite[splitSite.Length - 2]}/{pddPathPageUrl}/{pddPageName}.aspx", webpart, 1, 1, false);
                //var page = ctx.Web.GetFileByServerRelativeUrl($"{pddPathPageUrl}{pddPageName}.aspx");
                var page = ctx.Web.GetFileByServerRelativeUrl($"{pddPathPageUrl}{pddPageName}.aspx");
                var webPartManager = page.GetLimitedWebPartManager(PersonalizationScope.Shared);
                var importedWebPart = webPartManager.ImportWebPart(webPartXmlName);
                var webPart = webPartManager.AddWebPart(importedWebPart.WebPart, "wpz", 0);
                ctx.Load(webPart);
                ctx.ExecuteQuery();

                string marker = String.Format(CultureInfo.InvariantCulture, "<div class=\"ms-rtestate-read ms-rte-wpbox\" contentEditable=\"false\"><div class=\"ms-rtestate-read {0}\" id=\"div_{0}\"></div><div style='display:none' id=\"vid_{0}\"></div></div>", webPart.Id);
                ListItem item = page.ListItemAllFields;
                ctx.Load(item);
                ctx.ExecuteQuery();
                item["PublishingPageContent"] = marker;
                //item["WikiField"] = marker;
                item.Update();
                ctx.ExecuteQuery();

                #endregion

                #region Checkin Publish Page
                file = ctx.Web.GetFileByServerRelativeUrl($"{pddPathPageUrl}{pddPageName}.aspx");
                //dd.CheckOut();
                file.CheckIn("", CheckinType.MajorCheckIn);
                ctx.ExecuteQuery();

                #endregion
            }

            return $"Add PageName: {pddPageName} and Document Library: {pddDocumentName} Complete";
        }
        catch (ArgumentNullException ex)
        {
            return ex.Message;
        }
        catch (ArgumentException ex)
        {
            return ex.Message;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    public static void Test()
    {
        //using (var CC = new Microsoft.SharePoint.Client.ClientContext("https://pttgcgroup.sharepoint.com/sites/project/TPX/project/")) //https://pttgcgroup.sharepoint.com/NueiTest/,http://experimentweb.farm1.com , https://pttgcgroup.sharepoint.com , 
        //{
        using (ClientContext CC = new AuthenticationManager().GetAppOnlyAuthenticatedContext(_siteUrl, _username, _password))
        {
            //var webClient = new WebClient();
            //SecureString passWord = new SecureString();
            //foreach (char c in "P@ssword000".ToCharArray()) passWord.AppendChar(c);
            //webClient.Credentials = new NetworkCredential("z0006000", "P@ssword000", "pttgc");
            //CC.Credentials = new SharePointOnlineCredentials("z0006000@pttgcgroup.com", passWord);//new NetworkCredential("z0006000@pttgcgroup.com", "P@ssword000");

            //Without Online
            //CC.Credentials = new NetworkCredential("zkrihsana.n", "pass@word1", "pttdigital");

            //                var page = CC.Web.GetFileByServerRelativeUrl("/NueiTest/SitePage/test2.aspx");
            var page = CC.Web.GetFileByServerRelativeUrl("/sites/project/TPX/project/Pages/prodPageName.aspx");
            //var page = CC.Web.GetFileByServerRelativeUrl("/sites/TOP_LabDataCenter/SitePages/ddd.aspx");
            var webPartManager = page.GetLimitedWebPartManager(PersonalizationScope.Shared);

            var webPartXml = System.IO.File.ReadAllText("D:/PTT/PDDLibrary/WebPartZone/test2.webpart");
            //string webPartXml = ""

            var importedWebPart = webPartManager.ImportWebPart(webPartXml);
            var webPart = webPartManager.AddWebPart(importedWebPart.WebPart, "wpz", 0);
            CC.Load(webPart);
            CC.ExecuteQuery();

            string marker = String.Format(CultureInfo.InvariantCulture, "<div class=\"ms-rtestate-read ms-rte-wpbox\" contentEditable=\"false\"><div class=\"ms-rtestate-read {0}\" id=\"div_{0}\"></div><div style='display:none' id=\"vid_{0}\"></div></div>", webPart.Id);
            ListItem item = page.ListItemAllFields;
            CC.Load(item);
            CC.ExecuteQuery();
            item["PublishingPageContent"] = marker;
            //item["WikiField"] = marker;
            item.Update();
            CC.ExecuteQuery();

        }
    }
}